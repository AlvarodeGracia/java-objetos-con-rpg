package rpg;

public class Arma extends Objeto{
	
	private int ataque;
	private int da�o;
	
	public Arma(String nombre, int peso, int coste, int ataque, int da�o) {
		super(nombre, peso, coste);
		this.ataque = ataque;
		this.da�o = da�o;
	}
	
	public Arma() {
		super();
		this.ataque = 0;
		this.da�o = 0;
	}

	public int getAtaque() {
		return ataque;
	}
	
	public void setAtaque(int ataque) {
		this.ataque = ataque;
	}
	
	public int getDa�o() {
		return da�o;
	}
	
	public void setDa�o(int da�o) {
		this.da�o = da�o;
	}

	@Override
	public String toString() {
		return super.toString() +" Arma [ataque=" + ataque + ", da�o=" + da�o + "]";
	}
	
	
}
