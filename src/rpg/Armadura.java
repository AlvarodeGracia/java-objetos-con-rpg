package rpg;

public class Armadura extends Objeto{

	private int defensa;

	public Armadura(String nombre, int peso, int coste,int defensa) {
		super(nombre,peso,coste);
		this.defensa = defensa;
	}
	
	public Armadura() {
		super();
		this.defensa = 0;
	}

	public int getDefensa() {
		return defensa;
	}

	public void setDefensa(int defensa) {
		this.defensa = defensa;
	}

	@Override
	public String toString() {
		return super.toString()+" Armadura [defensa=" + defensa + "]";
	}
	
	
}
