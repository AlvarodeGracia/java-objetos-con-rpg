package rpg;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		ahoracado();
		
		Scanner teclado = new Scanner(System.in);
		
		Objeto nose = ObjetoFactory.createObjeto("espada");
		
		Arma espada = (Arma)ObjetoFactory.createObjeto("espada");
		Armadura armadura_cuero = (Armadura)ObjetoFactory.createObjeto("armadura cuero");
		
		Personaje heroe = new Personaje("Kalaur");
		heroe.setVida(15);
		
		heroe.equipar(espada);
		heroe.equipar(armadura_cuero);
		
		heroe.getMochila().add(espada);
		heroe.getMochila().add(armadura_cuero);
		
		//heroe.getMochila().stream().forEach(objeto -> System.out.println(objeto.toString()));
		
	}
	
	
	public static void test()
	{
		Scanner teclado = new Scanner(System.in);
		Arma espada = new Arma("Espada", 2, 50, 40 , 3);
		
		Personaje heroe = new Personaje("Kalaur");
		heroe.setVida(15);
		
		
		Personaje enemigo = new Personaje("Orco");
		enemigo.setFuerza(enemigo.getFuerza() - 2);

		int turno = 1;
		
		bucle_combate:
		do
		{
			System.out.println("Turno: "+turno);
			System.out.println("1-Atacar");
			System.out.println("2-Huir");
			System.out.println("3-Pocion");
			int opcion = teclado.nextInt();
			switch(opcion)
			{
				case 1:
					heroe.Combatir(enemigo);
					break;
				case 2:
					if(heroe.huir(turno))
						break bucle_combate;
					break;
				case 3:
					heroe.pocion();
					break;
			}
			if(enemigo.isMuerto())
				break;
			enemigo.Combatir(heroe);
			turno++;
		}while(!heroe.isMuerto() && !enemigo.isMuerto());
		
		System.out.println("Combate Terminado");
		teclado.close();
	}
	
	
	public static void ahoracado()
	{
		
		int vidas = 3;
		
		System.out.println("Inserte una palabra \n");
		Scanner teclado = new Scanner(System.in);
		String palabra = teclado.nextLine();
		char[] resultado =  new char[palabra.length()];
		boolean adivinado = false;
		
		for(int i = 0; i < palabra.length(); i++)
			resultado[i] = '*';
		
		System.out.println(resultado);
		
		int asteriscos;
		boolean isAcertado;
		do
		{
			asteriscos = 0;
			isAcertado = false;
			
			System.out.println("Inserte una letra \n");
			char letra = teclado.next().charAt(0);
			for(int i = 0; i < palabra.length(); i++)
			{
				if(palabra.charAt(i) == letra)
				{
					resultado[i] = letra;
					isAcertado = true;
				}
				if(resultado[i] == '*')
					asteriscos++;
			}
			
			if(isAcertado == false)
			{
				vidas--;
				System.out.println("Has perdido una vida te quedan: "+vidas);
			}
			
			if(asteriscos == 0)
				adivinado = true;
				
			System.out.println(resultado);
		}while(!adivinado && vidas > 0);
		
		if(vidas == 0)
		{
			System.out.println("Has perdido");
		}
		else
		{
			System.out.println("Has ganado");
		}
		
		
	}

}
