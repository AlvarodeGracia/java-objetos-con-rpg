package rpg;

public class Objeto {

	private String nombre;
	private int peso;
	private int coste;
	
	public Objeto() {	
		this.nombre = "";
		this.peso = 0;
		this.coste = 0;
	}
	
	
	public Objeto(String nombre, int peso, int coste) {
		this.nombre = nombre;
		this.peso = peso;
		this.coste = coste;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getPeso() {
		return peso;
	}


	public void setPeso(int peso) {
		this.peso = peso;
	}


	public int getCoste() {
		return coste;
	}


	public void setCoste(int coste) {
		this.coste = coste;
	}


	@Override
	public String toString() {
		return "Objeto [nombre=" + nombre + ", peso=" + peso + ", coste=" + coste + "]";
	}
	
	
	
}
