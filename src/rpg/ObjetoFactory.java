package rpg;

public class ObjetoFactory {

	public static Objeto createObjeto(String nombre)
	{
		Objeto objeto = null;
		
		switch(nombre)
		{
			case "espada":
				objeto = new Arma("Espada", 2, 50, 40 , 3);
				break;
			case "armadura cuero":
				objeto = new Armadura("Armadura de Cuero", 1, 25, 10);
				break;
			case "armadura malla":
				objeto = new Armadura("Armadura de Malla", 2, 50, 20);
				break;
			case "estatua":
				objeto = new Objeto("Estatua", 1, 10);
				break;
		}
		
		return (objeto);
	}	
}
