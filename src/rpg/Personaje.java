package rpg;

import java.util.ArrayList;

public class Personaje {
	
	private String nombrePersonaje;
	private int maxVida;
	private int vida;
	private int defensa;
	private int agilidad;
	private int fuerza;
	private int pociones;
	private ArrayList<Objeto> mochila;
	private Arma mano_derecha;
	private Armadura cuerpo;
	
	public Personaje()
	{
		nombrePersonaje = "";
		vida = 10;
		defensa = 25;
		agilidad = 20;
		fuerza = 20;
		pociones = 5;
		maxVida = vida;
		mochila = new ArrayList<Objeto>();
		cuerpo = null;
		mano_derecha = null;
	}
	
	public Personaje(String nombrePersonaje, int vida, int defensa, int agilidad, int fuerza, int pociones) {
		super();
		this.nombrePersonaje = nombrePersonaje;
		this.vida = vida;
		this.defensa = defensa;
		this.agilidad = agilidad;
		this.fuerza = fuerza;
		this.pociones = pociones;
		maxVida = this.vida;
		mochila = new ArrayList<Objeto>();
		cuerpo = null;
		mano_derecha = null;
	}

	public Personaje(String nombrePersonaje)
	{
		this();
		this.nombrePersonaje = nombrePersonaje;
	}


	public void Combatir(Personaje enemigo)
	{
		if(this.atacar())
		{
			if(!enemigo.defender())
			{
				enemigo.agregarVida(-this.getMano_derecha().getDa�o());
			}
		}
	}
	

	public boolean atacar() {

		boolean conseguido = false;
		int impacto = fuerza + mano_derecha.getAtaque();
		int dadoAtaque = Dado.tiradaD100();
		String resultado = nombrePersonaje + " no ha conseguido impactar";

		if (dadoAtaque < impacto)
		{
			resultado = nombrePersonaje + " ha acertado el impacto";
			conseguido = true;
		}

		System.out.println(resultado);
		return conseguido;
	}
	
	public boolean defender () {
		boolean conseguido = false;
		int dadoDefensa = Dado.tiradaD100();
		String resultado = nombrePersonaje + " no ha conseguido evitar el ataque.";

		if (dadoDefensa < defensa) {
			resultado = nombrePersonaje + " ha conseguido evitar el ataque";
			conseguido = true;
		}
		System.out.println(resultado);
		return conseguido;
	}
	
	public boolean huir(int turno) {
		boolean conseguido = false;
		int dadoHuir = Dado.tiradaD100();
		dadoHuir += 60 - turno * 10;
		String resultado = nombrePersonaje + " no ha conseguido escapar del combate. �Lucha cobarde!";
		
		if (dadoHuir < agilidad) 
		{
			resultado = nombrePersonaje + " ha conseguido escapar del combate";
			conseguido = true;
		}
		System.out.println(resultado);
		return conseguido;	
	}
	
	public void pocion () {
		
		if (pociones > 0) {
			pociones--;
			agregarVida(3);
			System.out.println(nombrePersonaje + " ha gastado una poci�n, le quedan "
			+ pociones + " y ahora tiene "+ vida + " puntos de vida");
		} else {
			System.out.println("No quedan pociones");
		}
		
	}
	
	public boolean isMuerto()
	{
		boolean isMuerto = false;
		
		if(this.vida <= 0)
		{
			isMuerto = true;
		}
		
		return (isMuerto);
	}

	public void agregarVida(int vida)
	{
		this.vida += vida;
		
		if(this.vida > maxVida)
			this.vida = maxVida;
		
		System.out.println(nombrePersonaje + " le quedan "+this.vida+" vidas.");
	}
	
	public void equipar(Objeto objeto)
	{
		if( objeto instanceof Arma)
			mano_derecha = (Arma)objeto;
		else if( objeto instanceof Armadura)
			cuerpo = (Armadura)objeto;
	}
	
	
	//GETTER SETTER
	public String getNombrePersonaje() {
		return nombrePersonaje;
	}

	public void setNombrePersonaje(String nombrePersonaje) {
		this.nombrePersonaje = nombrePersonaje;
	}

	public int getVida() {
		return vida;
	}

	public void setVida(int vida) {
		if(vida < 0)
			vida = 0;
		this.vida = vida;
		this.maxVida = vida;
	}

	public int getDefensa() {
		return defensa;
	}

	public void setDefensa(int defensa) {
		this.defensa = defensa;
	}

	public int getAgilidad() {
		return agilidad;
	}

	public void setAgilidad(int agilidad) {
		this.agilidad = agilidad;
	}

	public int getFuerza() {
		return fuerza;
	}

	public void setFuerza(int fuerza) {
		this.fuerza = fuerza;
	}

	public int getPociones() {
		return pociones;
	}

	public void setPociones(int pociones) {
		this.pociones = pociones;
	}

	public Arma getMano_derecha() {
		return mano_derecha;
	}

	public void setMano_derecha(Arma mano_derecha) {
		this.mano_derecha = mano_derecha;
	}


	public ArrayList<Objeto> getMochila() {
		return mochila;
	}

	public void setMochila(ArrayList<Objeto> mochila) {
		this.mochila = mochila;
	}

	public int getMaxVida() {
		return maxVida;
	}

	public void setMaxVida(int maxVida) {
		this.maxVida = maxVida;
	}

	public Armadura getCuerpo() {
		return cuerpo;
	}

	public void setCuerpo(Armadura cuerpo) {
		this.cuerpo = cuerpo;
	}
	
	
}

